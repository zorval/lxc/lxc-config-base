#!/bin/bash

################################################################################
# BSD 3-Clause License
# 
# Copyright (c) 2018-2019, Alban Vidal <alban.vidal@zordhak.fr>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# 
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# 
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
################################################################################

################################################################################
##########                    Define color to output:                 ########## 
################################################################################
_WHITE_="tput sgr0"
_RED_="tput setaf 1"
_GREEN_="tput setaf 2"
_ORANGE_="tput setaf 3"
_BLUE_="tput setaf 4"
################################################################################

# Path of git repository
# ./
echo "$($_ORANGE_)Define PATH of git repository$($_WHITE_)"
GIT_PATH="$(realpath ${0%/*})"
echo "Git PATH = $GIT_PATH"

################################################################################

# Test if basic configuration is done
if [ ! -f $GIT_PATH/config_99_VARS_LOCAL ] ; then
    echo "$($_RED_)ERROR - Local vars are not set, please launch 01_install$($_WHITE_)"
    exit 1
fi

################################################################################

# Load Network Vars
echo "$($_ORANGE_)source variables: $GIT_PATH/config_01_VARS_NETWORK$($_WHITE_)"
source $GIT_PATH/config_01_VARS_NETWORK

# Load Other vars 
echo "$($_ORANGE_)source variables: $GIT_PATH/config_02_VARS_OTHERS$($_WHITE_)"
source $GIT_PATH/config_02_VARS_OTHERS

# Source local vars
echo "$($_ORANGE_)source variables: $GIT_PATH/config_99_VARS_LOCAL$($_WHITE_)"
source $GIT_PATH/config_99_VARS_LOCAL

################################################################################

# overwritable variables
echo "$($_ORANGE_)Load overwritable variables$($_WHITE_)"
LXC_START_AUTO=${LXC_START_AUTO:=1}
echo "LXC_START_AUTO = $LXC_START_AUTO"
LXC_START_ORDER=${LXC_START_ORDER:=10}
echo "LXC_START_ORDER = $LXC_START_ORDER"
LXC_LIMIT_MEMORY=${LXC_LIMIT_MEMORY:=256}
echo "LXC_LIMIT_MEMORY = ${LXC_LIMIT_MEMORY}Mo"
FS_DATA_SIZE=${FS_DATA_SIZE:=2}
echo "FS_DATA_SIZE = ${FS_DATA_SIZE}Go"
#
# FS_EXTRA - Only for ZFS
FS_EXTRA=${FS_EXTRA:=false}

# Show options only if FS_EXTRA is enabled
if $FS_EXTRA ; then
    echo "FS_EXTRA = $FS_EXTRA"
    FS_EXTRA_SIZE=${FS_EXTRA_SIZE:=2}
    echo "FS_EXTRA_SIZE = ${FS_EXTRA_SIZE}Go"
    FS_EXTRA_PATH=${FS_EXTRA_PATH:=/srv/lxc/${NAME}_data}
    echo "FS_EXTRA_PATH = $FS_EXTRA_PATH"
    FS_EXTRA_PATH_CT=${FS_EXTRA_PATH_CT:=srv/data}
    echo "FS_EXTRA_PATH_CT = $FS_EXTRA_PATH_CT"
    FS_EXTRA_OPTS=${FS_EXTRA_OPTS:="rw,bind,create=dir"}
    echo "FS_EXTRA_OPTS = $FS_EXTRA_OPTS"
fi

################################################################################

# Test if storage allowed
declare -A STORAGE_TYPE_ALLOWED
STORAGE_TYPE_ALLOWED["ZFS"]=true
STORAGE_TYPE_ALLOWED["DIR"]=true

if [[ ! ${STORAGE_TYPE_ALLOWED[$STORAGE_TYPE]} ]] ; then
    echo echo "$($_RED_)ERROR: '$STORAGE_TYPE' is not a allowed storage$($_WHITE_)"
    exit 1
fi

################################################################################

if [ -z $NAME ] ; then
    echo "$($_RED_)ERROR: name does not defined - EXIT$($_WHITE_)"
    exit 1
fi

echo "$($_ORANGE_)Test if container $NAME already exist$($_WHITE_)"
if [ $(lxc-ls -f --filter=^$NAME$|wc -l) -ne 0 ] ; then
    echo "$($_RED_)ERROR: container $NAME already exist"
    exit 1
fi

################################################################################

echo "$($_ORANGE_)Create LXC configuration file for container $NAME$($_WHITE_)"
mkdir -p $GIT_PATH/ct-conf

cp -v /srv/git/lxc-config-base/templates/lxc-template $GIT_PATH/ct-conf/$NAME

sed -i                                      \
    -e "s/__IP_PUB__/$IP_PUB/"              \
    -e "s/__CIDR_PUB__/$CIDR_PUB/"          \
    -e "s/__IP_PUB_HOST__/$IP_PUB_HOST/"    \
    -e "s/__IP_PRIV__/$IP_PRIV/"            \
    -e "s/__CIDR_PRIV__/$CIDR_PRIV/"        \
    -e "s/__NAME__/$NAME/"                  \
    $GIT_PATH/ct-conf/$NAME

################################################################################

echo "$($_ORANGE_)Create local volume to mount into container $NAME$($_WHITE_)"

case $STORAGE_TYPE in
    "LVM" )
        echo TODO
        ;;
    "ZFS" )
        zfs create                                  \
            -o quota=${FS_DATA_SIZE}G               \
            -o mountpoint=/srv/lxc/$NAME            \
            ${ZFS_POOL}/srv_lxc_$NAME
        ;;
    "DIR" )
        mkdir -pv /srv/lxc/$NAME
        ;;
    * )
        echo "$($_RED_)ERROR - '$STORAGE_TYPE' is not a know storage type$($_WHITE_)"
        exit 1
        ;;
esac

################################################################################

# Only for ZFS
echo "$($_ORANGE_)Create local extra volume (if enable - only for ZFS)$($_WHITE_)"
if [[ $STORAGE_TYPE == "ZFS" ]] && [[ $FS_EXTRA == "true" ]] ; then
    echo "$($_ORANGE_)Extra volume is enabled$($_WHITE_)"
    DATASET_NAME="${ZFS_POOL}/$(echo $FS_EXTRA_PATH | sed 's#/#_#g'|sed 's/^_//')"
    FS_EXTRA_PATH_CT=$(echo $FS_EXTRA_PATH_CT | sed 's#^/##')
    zfs create                          \
        -o quota=${FS_EXTRA_SIZE}G      \
        -o mountpoint=$FS_EXTRA_PATH    \
        $DATASET_NAME
    echo "
# Extra data directory
lxc.mount.entry = $FS_EXTRA_PATH $FS_EXTRA_PATH_CT none $FS_EXTRA_OPTS 0 0
" >> $GIT_PATH/ct-conf/$NAME
else
    echo "$($_ORANGE_)Info: local extra volume is disable or stoage backend is not ZFS$($_WHITE_)"
fi

################################################################################

echo "$($_ORANGE_)Create container $NAME$($_WHITE_)"
case $STORAGE_TYPE in
    "LVM" )
        echo TODO
        ;;
    "ZFS" )
        lxc-create                                  \
            --quiet                                 \
            --bdev zfs                              \
            --zfsroot=${ZFS_POOL}/lxc               \
            -n $NAME                                \
            --config=$GIT_PATH/ct-conf/$NAME        \
            -t debian                               \
            --                                      \
            -r $DEBIAN_RELEASE
        ;;
    "DIR" )
        lxc-create                                  \
            --quiet                                 \
            -n $NAME                                \
            --config=$GIT_PATH/ct-conf/$NAME        \
            -t debian                               \
            --                                      \
            -r $DEBIAN_RELEASE
        ;;
    * )
        echo "$($_RED_)ERROR - '$STORAGE_TYPE' is not a know storage type$($_WHITE_)"
        exit 1
        ;;
esac


################################################################################

echo "$($_ORANGE_)Container $NAME - Start$($_WHITE_)"
lxc-start -n $NAME

################################################################################

echo "sleep...."

sleep 2

################################################################################

echo "$($_ORANGE_)Container $NAME - Update, upgrade and install common packages$($_WHITE_)"

PACKAGES='git vim apt-utils bsd-mailx postfix cron'
lxc-attach -n $NAME -- <<< "
    apt-get update -qq
    DEBIAN_FRONTEND=noninteractive apt-get -y install $PACKAGES > /dev/null
    mkdir -p /srv/git
    git clone --depth=1 https://framagit.org/zorval/config_system/basic_config_debian.git /srv/git/basic_config_debian
    # Setup config file for auto configuration
    >                                                   /srv/git/basic_config_debian/conf
    echo 'UNATTENDED_EMAIL=\"$TECH_ADMIN_EMAIL\"'    >> /srv/git/basic_config_debian/conf
    echo 'GIT_USERNAME=\"$FQDN\"'                    >> /srv/git/basic_config_debian/conf
    echo 'GIT_EMAIL=\"root@$DOMAIN_NAME\"'           >> /srv/git/basic_config_debian/conf
    echo 'SSH_EMAIL_ALERT=\"$TECH_ADMIN_EMAIL\"'     >> /srv/git/basic_config_debian/conf
    # Launch auto configuration script
    /srv/git/basic_config_debian/auto_config.sh
"

################################################################################

echo "TODO : cron ?"

################################################################################

# Postfix default conf file
echo "$($_ORANGE_)Container $NAME - Edit postfix configuration file$($_WHITE_)"

cp -v $GIT_PATH/templates/all/etc/postfix/main.cf /srv/lxc/$NAME/etc_postfix_main.cf

sed -i                                      \
    -e "s/__DOMAIN_NAME__/$DOMAIN_NAME/"    \
    -e "s/__IP_PRIV_SMTP__/$IP_PRIV_SMTP/"  \
    /srv/lxc/$NAME/etc_postfix_main.cf

lxc-attach -n $NAME -- <<< "
    echo '(in container)'
    cp -v /srv/lxc/etc_postfix_main.cf /etc/postfix/main.cf
"

################################################################################

echo "$($_ORANGE_)Container $NAME - Add container name in /etc/hosts$($_WHITE_)"
lxc-attach -n $NAME -- <<< "
    echo '$IP_PUB       $NAME' >> /etc/hosts
"

################################################################################

# caping memory
echo "$($_ORANGE_)Container $NAME - caping memory$($_WHITE_)"
sed -i                                                                          \
    -e "s/#\(lxc.cgroup.memory.limit_in_bytes =\).*/\1 ${LXC_LIMIT_MEMORY}M/"   \
    /var/lib/lxc/$NAME/config

# define if container autostart at boot
echo "$($_ORANGE_)Container $NAME - define if container autostart at boot$($_WHITE_)"
sed -i                                                      \
    -e "s/^\(lxc.start.auto =\).*/\1 $LXC_START_AUTO/"      \
    /var/lib/lxc/$NAME/config

# define order on reboot
echo "$($_ORANGE_)Container $NAME - define order on reboot$($_WHITE_)"
sed -i                                                      \
    -e "s/^\(lxc.start.order =\).*/\1 $LXC_START_ORDER/"    \
    /var/lib/lxc/$NAME/config

################################################################################

# restart
echo "$($_ORANGE_)Container $NAME - Restart container$($_WHITE_)"
lxc-stop --reboot -n $NAME

################################################################################
